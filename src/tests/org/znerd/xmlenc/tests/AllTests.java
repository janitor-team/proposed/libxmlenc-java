/*
 * $Id: AllTests.java,v 1.7 2005/09/12 08:40:08 znerd Exp $
 */
package org.znerd.xmlenc.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * Combination of all tests.
 *
 * @version $Revision: 1.7 $ $Date: 2005/09/12 08:40:08 $
 * @author Ernst de Haan (<a href="mailto:wfe.dehaan@gmail.com">wfe.dehaan@gmail.com</a>)
 */
public class AllTests extends TestSuite {

   //-------------------------------------------------------------------------
   // Class functions
   //-------------------------------------------------------------------------

   /**
    * Returns a test suite with all test cases.
    *
    * @return
    *    the test suite, never <code>null</code>.
    */
   public static Test suite() {
      TestSuite suite = new TestSuite();
      suite.addTestSuite(XMLCheckerTests.class);
      suite.addTestSuite(XMLOutputterTests.class);
      suite.addTestSuite(OutputTests.class);
      return suite;
   }


   //-------------------------------------------------------------------------
   // Constructor
   //-------------------------------------------------------------------------

   /**
    * Constructs a new <code>AllTests</code> object with the specified name.
    * The name will be passed to the superconstructor.
    *
    * @param name
    *    the name for this test case.
    */
   public AllTests(String name) {
      super(name);
   }
}
